{ pkgs, ... }:
pkgs.mkShell {
  buildInputs = [ pkgs.nodejs pkgs.sass ];
  shellHook = ''
  '';
  buildPhase = ''
    ${pkgs.sass}/bin/scss --style compressed style.scss > style.css
  '';
}
